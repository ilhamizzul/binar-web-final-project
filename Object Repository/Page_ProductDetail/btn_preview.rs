<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_preview</name>
   <tag></tag>
   <elementGuidId>49134639-176b-4e21-b75e-18a9f1571869</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@name='commit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[@class='btn btn-outline-primary w-50 rounded-4 p-3']</value>
      <webElementGuid>8124c8e4-dd7a-49ad-9cc7-721e776b3a0c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
