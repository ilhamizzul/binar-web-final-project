<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_kategoriproduct</name>
   <tag></tag>
   <elementGuidId>1b3f8de7-929b-4615-85a5-032642b9b209</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[@class='card-body'])[2]/p[@class='card-text text-black-50 fs-6']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@class='card-body'])[2]/p[@class='card-text text-black-50 fs-6']</value>
      <webElementGuid>254c6147-4589-4f6b-a055-964b23092ca6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
