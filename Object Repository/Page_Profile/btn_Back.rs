<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Back</name>
   <tag></tag>
   <elementGuidId>b182109d-d4b4-446b-b634-f1752fbb01ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='btn btn-link']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='btn btn-link']</value>
      <webElementGuid>5c16d4ec-9505-4e9c-a782-e8896d2a1cd9</webElementGuid>
   </webElementProperties>
</WebElementEntity>
