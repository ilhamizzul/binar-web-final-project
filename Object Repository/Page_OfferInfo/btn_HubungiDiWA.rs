<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_HubungiDiWA</name>
   <tag></tag>
   <elementGuidId>737a8d49-7042-43c5-9f7b-1182dc0f8906</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//a[@class='btn btn-primary fw-bold rounded-pill px-4'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//a[@class='btn btn-primary fw-bold rounded-pill px-4'])[1]</value>
      <webElementGuid>9131572a-0a85-4272-b10c-3df9dd4575d8</webElementGuid>
   </webElementProperties>
</WebElementEntity>
