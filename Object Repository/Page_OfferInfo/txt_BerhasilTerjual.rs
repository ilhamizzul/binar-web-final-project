<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_BerhasilTerjual</name>
   <tag></tag>
   <elementGuidId>48713681-ce0c-4d06-a795-e08cedaeb4d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//h6[normalize-space()='Penjualan dibatalkan'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//h6[normalize-space()='Penjualan dibatalkan'])[1]</value>
      <webElementGuid>b89fac61-7e2d-460c-975f-019dcd7be3c0</webElementGuid>
   </webElementProperties>
</WebElementEntity>
