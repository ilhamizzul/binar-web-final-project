<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>rdbtn_BatalTransaksi</name>
   <tag></tag>
   <elementGuidId>35af6d92-4638-4f37-9dc7-dc92478a1b22</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='modal show']//input[@id='offer_status_cancelled']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='modal show']//input[@id='offer_status_cancelled']</value>
      <webElementGuid>0348970d-2655-476a-8297-79d4f6089280</webElementGuid>
   </webElementProperties>
</WebElementEntity>
