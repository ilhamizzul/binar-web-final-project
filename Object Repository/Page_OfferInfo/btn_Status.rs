<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Status</name>
   <tag></tag>
   <elementGuidId>f45767d7-f883-42c1-8b20-0785d9f51bfe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@class='btn btn-outline-primary fw-bold rounded-pill px-4'])[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>(//button[@class='btn btn-outline-primary fw-bold rounded-pill px-4'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//button[@class='btn btn-outline-primary fw-bold rounded-pill px-4'])[1]</value>
      <webElementGuid>da6ec2e0-f840-4300-a027-9c5158be9376</webElementGuid>
   </webElementProperties>
</WebElementEntity>
