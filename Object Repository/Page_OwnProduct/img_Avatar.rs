<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_Avatar</name>
   <tag></tag>
   <elementGuidId>a827f1cb-36ef-4c41-b477-c114f9fa5fa8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/img[@class='img-avatar w-100 rounded-4']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
