<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_CTAEditProfile</name>
   <tag></tag>
   <elementGuidId>f1bb5664-81fe-4e82-882b-1d45f394b8fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[normalize-space()='Edit']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
