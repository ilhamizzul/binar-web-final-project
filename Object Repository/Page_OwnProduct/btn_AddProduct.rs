<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>btn_AddProduct</description>
   <name>btn_AddProduct</name>
   <tag></tag>
   <elementGuidId>60a03be0-9ec1-4b89-887b-47f66ab2cb58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h5[normalize-space()='Tambah Produk']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
