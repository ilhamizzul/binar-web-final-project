<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>hero title on login page</description>
   <name>ttl_Hero</name>
   <tag></tag>
   <elementGuidId>615e6873-efea-40c8-a2af-44a90edbdf08</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[(text() = 'SecondHand.' or . = 'SecondHand.')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
