<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icn_Profile</name>
   <tag></tag>
   <elementGuidId>8f1e5ac0-346f-4ffb-a290-836fedfb978a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@class='nav-link dropdown-toggle d-flex align-items-center']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@class='nav-link dropdown-toggle d-flex align-items-center']</value>
      <webElementGuid>b35df48b-caeb-49cd-bce2-c6f128edb1a4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
