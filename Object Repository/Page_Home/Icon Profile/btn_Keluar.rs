<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Keluar</name>
   <tag></tag>
   <elementGuidId>3110038a-7782-4a35-b249-e666ab6a25a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='btn btn-link text-decoration-none']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='btn btn-link text-decoration-none']</value>
      <webElementGuid>6cc890f4-e180-46f0-abc6-3fbe7a324c82</webElementGuid>
   </webElementProperties>
</WebElementEntity>
