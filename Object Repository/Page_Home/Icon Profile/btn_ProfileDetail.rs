<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_ProfileDetail</name>
   <tag></tag>
   <elementGuidId>27b019ff-5f77-403b-86ee-74ed35729296</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//ul[@class='dropdown-menu show']//a[@href='/profiles']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//ul[@class='dropdown-menu show']//a[@href='/profiles']</value>
      <webElementGuid>69f907f2-6f01-4dba-b930-c2d602744b0b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
