<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icn_Notifikasi</name>
   <tag></tag>
   <elementGuidId>04fd52c3-d30b-4584-9b4d-c79edc6374c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//i[@class='bi bi-bell']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//i[@class='bi bi-bell']</value>
      <webElementGuid>dc8558d0-fba1-418d-9e98-d8d1061f0495</webElementGuid>
   </webElementProperties>
</WebElementEntity>
