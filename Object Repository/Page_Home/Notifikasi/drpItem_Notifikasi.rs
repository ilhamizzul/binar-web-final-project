<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>drpItem_Notifikasi</name>
   <tag></tag>
   <elementGuidId>5d0b8103-c1ab-47f4-a9f1-538a68ac5496</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//ul[@class='dropdown-menu notification-dropdown-menu px-4 show']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//ul[@class='dropdown-menu notification-dropdown-menu px-4 show']</value>
      <webElementGuid>30258a35-758c-47a6-9ae0-73636a243752</webElementGuid>
   </webElementProperties>
</WebElementEntity>
