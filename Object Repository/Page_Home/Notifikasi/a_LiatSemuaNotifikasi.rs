<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_LiatSemuaNotifikasi</name>
   <tag></tag>
   <elementGuidId>e495553f-b6c1-4b61-b966-c52da865f420</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(.,'Lihat semua notifikasi')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(.,'Lihat semua notifikasi')]</value>
      <webElementGuid>6b5d9e18-e01b-412b-b5ed-632e30abf2ff</webElementGuid>
   </webElementProperties>
</WebElementEntity>
