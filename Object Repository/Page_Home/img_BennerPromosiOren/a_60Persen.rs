<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_60Persen</name>
   <tag></tag>
   <elementGuidId>17816f72-7a2f-4f66-9126-f95925b1506d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h5[@class='display-6 text-danger fw-bold']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h5[@class='display-6 text-danger fw-bold']</value>
      <webElementGuid>1e766f6f-38a7-4e7d-b371-6332a636d8c6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
