<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>bgrn_ImageBennerPromosiOren</name>
   <tag></tag>
   <elementGuidId>88877b70-da87-43ff-8355-b8ac750ed11a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>&lt;div class=&quot;promo center flex-grow-1 bg-warning rounded-lg-4 rounded-0&quot;></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>&lt;div class=&quot;promo center flex-grow-1 bg-warning rounded-lg-4 rounded-0&quot;></value>
      <webElementGuid>08f8dc70-ae53-48fa-a79f-3d732a4aaff4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
