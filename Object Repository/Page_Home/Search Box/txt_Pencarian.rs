<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_Pencarian</name>
   <tag></tag>
   <elementGuidId>fa553638-f470-429e-9ac4-e15eacbb5bb0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@class='form-control bg-transparent border-0 form-search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@class='form-control bg-transparent border-0 form-search']</value>
      <webElementGuid>fe144e45-bc7c-41af-b69d-cad2bf7e12cb</webElementGuid>
   </webElementProperties>
</WebElementEntity>
