<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icn_Pencarian</name>
   <tag></tag>
   <elementGuidId>a7b75093-524e-42fe-94c6-5573c3340f2f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//i[@class='bi bi-search input-group-text bg-transparent border-0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//i[@class='bi bi-search input-group-text bg-transparent border-0']</value>
      <webElementGuid>973aea4d-4d66-4ef7-a218-a4786a35f270</webElementGuid>
   </webElementProperties>
</WebElementEntity>
