package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class Login {
	@Given("user already open the secondhand login page")
	def user_already_open_the_secondhand_login_page() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Login/Pages/Open Login Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user want to login using correct credential of newly made account")
	def user_want_to_login_using_correct_credential_of_newly_made_account() {
		WebUI.callTestCase(findTestCase('Login/Pages/Input Email'), [('email') : 'demoizzul@gmail.com'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Login/Pages/Input Password'), [('password') : 'demo123'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Login/Pages/Click Submit Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user want to login using seller account")
	def user_want_to_login_using_seller_account() {
		WebUI.callTestCase(findTestCase('Login/Pages/Input Email'), [('email') : 'user@gmail.com'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Login/Pages/Input Password'), [('password') : 'user123'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Login/Pages/Click Submit Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input using invalid login credential")
	def user_input_using_invalid_login_credential() {
		WebUI.callTestCase(findTestCase('Login/Pages/Input Email'), [('email') : 'INVALID@gmail.com'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Login/Pages/Input Password'), [('password') : 'qwerty123456'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Login/Pages/Click Submit Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user success to login and redirected into account info page")
	def user_success_to_login_and_redirected_into_account_info_page() {
		WebUI.callTestCase(findTestCase('Login/Pages/Verify Login Success'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user stay on loginpage and system show error notification")
	def user_stay_on_loginpage_and_system_show_error_notification() {
		WebUI.callTestCase(findTestCase('Login/Pages/Verify Login Failed'), [('errorMessage') : 'Invalid Email or password.'], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}


