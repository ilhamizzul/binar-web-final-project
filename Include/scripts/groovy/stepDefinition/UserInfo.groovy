package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class UserInfo {
	@Given("user already login and user on the main page")
	def user_already_login_and_on_the_main_page() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS01 User Login/TS-0102-P user login using completed data account'), [('email') : 'user@gmail.com'
			, ('password') : 'user123'], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("user already login and user on detail account info page")
	def user_already_login_and_on_detail_account_page() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS01 User Login/TS-0102-P user login using completed data account'), [('email') : 'user@gmail.com'
			, ('password') : 'user123'], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Homepage/page/Icon Profile Saya/Click Icon Profile Saya'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Homepage/page/Icon Profile Saya/Click Detail Profil'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user want to see detail account info")
	def user_want_to_see_detail_account_info() {
		WebUI.callTestCase(findTestCase('Homepage/page/Icon Profile Saya/Click Icon Profile Saya'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Homepage/page/Icon Profile Saya/Click Detail Profil'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user want to fill all the detail account info")
	def user_want_to_fill_all_the_detail_account_info() {
		String charsNumber = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
		String username = 'TESTER_USER_' + randomString(charsNumber, 5)
		WebUI.callTestCase(findTestCase('Profile/Pages/Upload Image'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Profile/Pages/Input Nama'), [('namalogin') : username], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Profile/Pages/Input Kota'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Profile/Pages/Input Alamat'), [('alamatlogin') : 'jl. bunga, kembangan, jakarta barat'],
		FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Profile/Pages/Input Nomor Handphone'), [('handphonelogin') : '083807969098'], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Profile/Pages/Click Button Simpan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user successfully redirected into detail account info page")
	def user_successfully_redirected_into_detail_account_info_page() {
		WebUI.callTestCase(findTestCase('Homepage/page/Verify Succes Accessed Account Details Page'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("all data should be successfully saved to system")
	def all_data_should_be_successfully_saved_to_system() {
		WebUI.callTestCase(findTestCase('Profile/Pages/Verify Success'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	static String randomString(String chars, int length) {
		Random rand = new Random()

		StringBuilder sb = new StringBuilder()

		for (int i = 0; i < length; i++) {
			sb.append(chars.charAt(rand.nextInt(chars.length())))
		}

		return sb.toString()
	}
}


