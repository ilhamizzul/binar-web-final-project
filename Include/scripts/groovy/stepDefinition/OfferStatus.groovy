package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class OfferStatus {

	@Given("seller already on product offering page")
	def seller_already_on_product_offering_page() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS09 Offering Product Page/TS-0901-P user want to see a product that offered by other user'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("seller want to reject product offer from other users")
	def seller_want_to_reject_product_offer_from_other_users() {
		WebUI.callTestCase(findTestCase('OfferInfo/pages/Click Tolak'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("offer should be rejected successfully")
	def offer_should_be_rejected_successfully() {
		WebUI.callTestCase(findTestCase('OfferInfo/pages/Verify Penawaran Tolak'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Given("there are already accepted offer")
	def there_are_already_accepted_offer() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS09 Offering Product Page/TS-0902-P user want to approve other account offer on a certain user product'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("seller want to reject accepted offer product")
	def seller_want_to_reject_accepted_offer_product() {
		WebUI.callTestCase(findTestCase('OfferInfo/pages/Click Status'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('OfferInfo/pages/Status/Click Batalkan Transaksi'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('OfferInfo/pages/Status/Click Kirim'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("seller want to finalize offer transaction with other users")
	def seller_want_to_finalize_offer_transaction_with_other_users() {
		WebUI.callTestCase(findTestCase('OfferInfo/pages/Click Status'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('OfferInfo/pages/Status/Click Berhasil terjual'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('OfferInfo/pages/Status/Click Kirim'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("offer transaction should be closed and done")
	def offer_transaction_should_be_closed_and_done() {
		WebUI.callTestCase(findTestCase('OfferInfo/pages/Verify Berhasil terjual'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

}