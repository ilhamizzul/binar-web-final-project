package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Logout {

	@Given("user already login")
	def user_already_login() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		//WebUI.callTestCase(findTestCase('Login/Pages/Open Login Page'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS01 User Login/TS-0102-P user login using completed data account'), [('email') : 'user@gmail.com'
			, ('password') : 'user123'], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user want to logout from the web application")
	def user_want_to_logout_from_the_web_application() {
		WebUI.callTestCase(findTestCase('Homepage/page/Icon Profile Saya/Click Icon Profile Saya'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Homepage/page/Icon Profile Saya/Click Keluar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user can successfully logout from the used account")
	def user_can_successfully_logout_from_the_used_account() {
		WebUI.callTestCase(findTestCase('Homepage/page/Verify Logout Succes'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}