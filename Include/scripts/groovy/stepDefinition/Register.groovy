package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Register {

	String chars = 'abcdefghijklmnopqrstuvwxyz0123456789'
	//TS0201
	@Given("user already on login page")
	def user_already_on_login_page() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Login/Pages/Open Login Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user want to register new account")
	def user_want_to_register_new_account() {
		WebUI.callTestCase(findTestCase('Login/Pages/Click Register'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user successfully redirected into register page")
	def user_successfully_redirected_into_register_page() {
		WebUI.callTestCase(findTestCase('Register/Pages/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}


	//TS0202
	@Given("user already on register page")
	def user_already_on_register_page() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Register/Pages/Open Register Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user register by filling all form correctly")
	def user_register_by_filling_all_form_correctly() {
		WebUI.callTestCase(findTestCase('Register/Pages/Input Nama'), [('nama') : 'TESTER_' + randomString(chars, 10)], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Register/Pages/Input Email'), [('email') : randomString(chars, 10) + '@gmail.com'], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Register/Pages/Input Password'), [('password') : randomString(chars, 12)], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Register/Pages/Click Submit Register'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user success create new account and registered into main page")
	def user_success_create_new_account_and_registered_into_main_page() {
		WebUI.callTestCase(findTestCase('Register/Pages/Verify Register Success'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	static String randomString(String chars, int length) {
		Random rand = new Random()

		StringBuilder sb = new StringBuilder()

		for (int i = 0; i < length; i++) {
			sb.append(chars.charAt(rand.nextInt(chars.length())))
		}

		return sb.toString()
	}
}