package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



public class BuyProduct {

	//TS 0601-user want to look detailed product info
	@Given("user already on the main page")
	def user_already_on_the_main_page() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS01 User Login/TS-0102-P user login using completed data account'), [('email') : 'zakkimubarak03@gmail.com'
			, ('password') : 'zakkibarca0310'], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user want to see detailed description of certain product")
	def user_want_to_see_detailed_description_of_certain_product() {
		WebUI.callTestCase(findTestCase('Homepage/page/Search Box/Input Search Box'), [('PencarianProduct') : 'TESTER_PRODUCT'],
		FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Homepage/page/Click Card Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("system should redirected user into detail product")
	def system_should_redirected_user_into_detail_product() {
		WebUI.callTestCase(findTestCase('ProductDetail/Pages/Verify Menu Product'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	//TS 0602-user want to create an offer to a certain product
	@Given("User sudah login dengan akunnya")
	def User_sudah_login_dengan_akunnya() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS01 User Login/TS-0102-P user login using completed data account'), [('email') : 'zakkimubarak03@gmail.com'
			, ('password') : 'zakkibarca0310'], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user mau beli")
	def user_mau_beli() {
		WebUI.callTestCase(findTestCase('Homepage/page/Click Card Product'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('ProductDetail/Pages/Click Nego'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('ProductDetail/Pages/Input Harga Nego'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("system successfully create an offer")
	def system_successfully_create_an_offer() {
		WebUI.callTestCase(findTestCase('ProductDetail/Pages/Verify Success Nego'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}