package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



public class MyProduct {
	
	String charsNumber = '0123456789'
	
	String productName = 'TESTER_PRODUCT_EDITED' + randomString(charsNumber, 5)
	
	String price = '15000'

	//TS 0801-P user want to see a product that user created
	@Given("user sudah login")
	def user_sudah_login() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS01 User Login/TS-0102-P user login using completed data account'), [('email') : 'user@gmail.com'
			, ('password') : 'user123'], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User ingin melihat product yang sudah di buat")
	def User_ingin_melihat_product_yang_sudah_di_buat() {
		WebUI.callTestCase(findTestCase('OwnProduct/Pages/Open Own Product Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user dapat melihat seluruh peruduk yang di jual")
	def user_dapat_melihat_seluruh_peruduk_yang_di_jual() {
		WebUI.callTestCase(findTestCase('OwnProduct/Pages/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	//TS 0802-P user want to see a detailed product that user created
	@Given("User sudah login")
	def User_sudah_login() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Step Definition/TS01 User Login/TS-0102-P user login using completed data account'), [('email') : 'user@gmail.com'
			, ('password') : 'user123'], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user dapat melihat detail info produk yang dijual")
	def user_dapat_melihat_detail_info_produk_yang_dijual() {
		WebUI.callTestCase(findTestCase('OwnProduct/Pages/Open Own Product Page'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('OwnProduct/Pages/Click Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("system mengarahkan user ke halaman detail product")
	def system_mengarahkan_user_ke_halaman_detail_product() {
		WebUI.callTestCase(findTestCase('ProductDetail/Pages/verify content own product'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	//TS 0803-P user want to edit a product data that user created
	@Given("User berada di halaman detail product")
	def User_berada_di_halaman_detail_product() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS08 My Product/TS-0802-P user want to see detailed info of created product'),
				[:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user ingin edit produk yang di jual")
	def user_ingin_edit_produk_yang_di_jual() {
		WebUI.callTestCase(findTestCase('ProductDetail/Pages/Click Edit'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Nama Produk'), [('productName') : productName], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Harga Produk'), [('productPrice') : price], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Select Kategori Produk'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Deskripsi Produk'), [('productDescription') : 'lorem ipsum dolor sit amet'],
		FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Click Create Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user berhasil edit product")
	def user_berhasil_edit_product() {
		WebUI.callTestCase(findTestCase('ProductDetail/Pages/verify content own product'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('ProductDetail/Pages/Verify Product Name'), [('productName') : productName], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	//TS 0805-P user want to delete a product that user created
	@Given("User berada dihalaman detail product")
	def User_berada_dihalaman_detail_product() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS08 My Product/TS-0802-P user want to see detailed info of created product'),
				[:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User ingin menghapus product yang dijual")
	def User_ingin_menghapus_product_yang_dijual() {
		String productName = WebUI.callTestCase(findTestCase('ProductDetail/Pages/Get Product Name'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('ProductDetail/Pages/Click Delete'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Sistem berhasil menghapus produt")
	def Sistem_berhasil_menghapus_produt() {
		WebUI.callTestCase(findTestCase('OwnProduct/Pages/Verify Product Deleted'), [('productName') : productName], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	static String randomString(String chars, int length) {
		Random rand = new Random()
	
		StringBuilder sb = new StringBuilder()
	
		for (int i = 0; i < length; i++) {
			sb.append(chars.charAt(rand.nextInt(chars.length())))
		}
		
		return sb.toString()
	}
}