package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class SellProduct {
	String charsNumber = '0123456789'
	String productName = 'TESTER_PRODUCT' + randomString(charsNumber, 5)
	String previewProductName = 'TESTER_PRODUCT' + randomString(charsNumber, 5)

	@Given("user is not login and on the main page")
	def user_is_not_login_and_on_the_main_page() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Homepage/page/Click Jual'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user want to create a product without an account")
	def user_want_to_create_product_without_account() {
		WebUI.callTestCase(findTestCase('Login/Pages/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user can't continue and redirected to login page with system warning notification")
	def user_cant_continue_and_redirected_to_login() {
		WebUI.callTestCase(findTestCase('Login/Pages/Verify Login Failed'), [('errorMessage') : 'You need to sign in or sign up before continuing.'], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("user already login and on the main page")
	def user_already_login_and_on_the_main_page() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS01 User Login/TS-0102-P user login using completed data account'), [('email') : 'user@gmail.com', ('password') : 'user123'], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user want to access create product form")
	def user_want_to_access_create_product_form() {
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Open Create Product Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will be redirected to sell product form page")
	def user_will_be_redirected_to_sell_product_form_page() {
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("user already login and on the create product form page")
	def user_already_login_and_on_the_create_product_form_page() {
		WebUI.callTestCase(findTestCase('Helper/Open Browser'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Step Definition/TS01 User Login/TS-0102-P user login using completed data account'), [('email') : 'user@gmail.com', ('password') : 'user123'], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Open Create Product Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user want to create a new product to sell")
	def user_want_to_create_a_new_product_to_sell() {
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Nama Produk'), [('productName') : productName], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Harga Produk'), [('productPrice') : '10000'], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Select Kategori Produk'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Deskripsi Produk'), [('productDescription') : 'lorem ipsum dolor sit amet'],
		FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Upload Image Produk'), [('fileURL') : ''], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Click Create Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user success to create a product and redirected to product detail")
	def user_success_to_create_new_product_to_sell() {
		WebUI.callTestCase(findTestCase('ProductDetail/Pages/verify content own product'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('ProductDetail/Pages/Verify Product Name'), [('productName') : productName], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user want to preview a new product to sell")
	def user_want_to_preview_a_new_product_to_sell() {
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Nama Produk'), [('productName') : previewProductName], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Harga Produk'), [('productPrice') : '10000'], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Select Kategori Produk'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Deskripsi Produk'), [('productDescription') : 'lorem ipsum dolor sit amet'],
		FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Upload Image Produk'), [('fileURL') : ''], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('CreateProduct/Pages/Click Preview'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user successfully redirected to product preview page")
	def user_success_to_preview_new_product_to_sell() {
		WebUI.callTestCase(findTestCase('ProductDetail/Pages/verify content preview page'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('ProductDetail/Pages/Verify Product Name'), [('productName') : previewProductName], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Helper/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	static String randomString(String chars, int length) {
		Random rand = new Random()

		StringBuilder sb = new StringBuilder()

		for (int i = 0; i < length; i++) {
			sb.append(chars.charAt(rand.nextInt(chars.length())))
		}

		return sb.toString()
	}
}


