@Register @2_Register
Feature: Register
  As a user, I want to Register in secondhand web binar

  @TS0201 @positive 
  Scenario: TS0201-P - user open register page
    Given user already on login page
    When user want to register new account
    Then user successfully redirected into register page

  @TS0202 @positive @E2E
  Scenario: TS0202-P - user want to register properly
    Given user already on register page
    When user register by filling all form correctly
    Then user success create new account and registered into main page


