@UserInfo
Feature: UserInfo
  As a user, I want to see and maintain my account info

  @TS0301 @positive
  Scenario: TS0301-P - user want to open detail account info
    Given user already login and user on the main page
    When user want to see detail account info
    Then user successfully redirected into detail account info page

  @TS0302 @positive @E2E
  Scenario: TS0302-P - user want to fiil detail account info
    Given user already login and user on detail account info page
    When user want to fill all the detail account info
    Then all data should be successfully saved to system
