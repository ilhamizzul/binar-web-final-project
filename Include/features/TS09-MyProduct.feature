@Productsaya
Feature: MyProduct
  As a user, I want to view, edit and delate Product

  @TS0901 @positive
  Scenario: TS0801-P - user want to see a product that user created
    Given User sudah login
    When User ingin melihat product yang sudah di buat
    Then user dapat melihat seluruh peruduk yang di jual

  @TS0902 @positive
  Scenario: TS0802-P - user want to see a detailed product that user created
    Given User sudah login
    When user dapat melihat detail info produk yang dijual
    Then system mengarahkan user ke halaman detail product

  @TS0903 @positive
  Scenario: TS0803-P - user want to edit a product data that user created
    Given User berada di halaman detail product
    When user ingin edit produk yang di jual
    Then user berhasil edit product

  @TS0905 @positive @6_DeleteProduct
  Scenario: TS0805-P - user want to delete a product that user created
    Given User berada dihalaman detail product
    When User ingin menghapus product yang dijual
    Then Sistem berhasil menghapus produt
