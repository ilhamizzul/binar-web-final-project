@Login @1_Login
Feature: Login
  As a user, I want to login in secondhand web binar

  @TS0101 @positive
  Scenario: TS0101-P - user want to login using newly made account
    Given user already open the secondhand login page
    When user want to login using correct credential of newly made account
    Then user success to login and redirected into account info page

  @TS0102 @positive @E2E
  Scenario: TS0102-P - user want to login using seller account
    Given user already open the secondhand login page
    When user want to login using seller account
    Then user success to login and redirected into account info page

  @TS0103 @negative
  Scenario: TS0103-N - user want to login using incorrect credential
    Given user already open the secondhand login page
    When user input using invalid login credential
    Then user stay on loginpage and system show error notification
