@Logout @7_Logout
Feature: Logout
  As a user, I want to Logout in secondhand web binar

  @TS0401 @positive
  Scenario: TS0401-P - User want to logout from the account
    Given user already login
    When user want to logout from the web application
    Then user can successfully logout from the used account