@BuyProduct
Feature: BuyProduct
  As a user, I want to Buy in secondhand web binar

  @TS0701 @positive
  Scenario: TS0601-P - user want to look detailed product info
    Given user already on the main page
    When user want to see detailed description of certain product
    Then system should redirected user into detail product

  @TS0702 @positive @4_E2E-SellerRejectOffer @5_E2E-SellerAcceptOffer
  Scenario: TS0602-P - user want to create an offer to a certain product
    Given User sudah login dengan akunnya
    When user mau beli
    Then system successfully create an offer

  @TS0803 @positive @4_E2E-SellerRejectOffer
  Scenario: user want to reject offer from other user
    Given seller already on product offering page
    When seller want to reject product offer from other users
    Then offer should be rejected successfully

  @TS0804 @positive @5_E2E-SellerAcceptOffer
  Scenario: user want to change approved offer status to sold successfully
    Given there are already accepted offer
    When seller want to finalize offer transaction with other users
    Then offer transaction should be closed and done

  @TS0805 @positive
  Scenario: user want to cancel approved offer
    Given there are already accepted offer
    When seller want to reject accepted offer product
    Then offer should be rejected successfully
