@SellProduct
Feature: SellProduct
  As a user, I want to create a product to sell

  @TS0601 @negative
  Scenario: TS0701-N - user can't sell a product without account
    Given user is not login and on the main page
    When user want to create a product without an account
    Then user can't continue and redirected to login page with system warning notification

  @TS0603 @positive
  Scenario: TS0703-P - user want to open sell product page
    Given user already login and on the main page
    When user want to access create product form
    Then user will be redirected to sell product form page

  @TS0604 @positive @3_E2E-CreateProduct
  Scenario: TS0704-P - user want to create a product
    Given user already login and on the create product form page
    When user want to create a new product to sell
    Then user success to create a product and redirected to product detail

  @TS0607 @positive
  Scenario: TS0704-P - user want to preview a product before submiting into system
    Given user already login and on the create product form page
    When user want to preview a new product to sell
    Then user successfully redirected to product preview page
