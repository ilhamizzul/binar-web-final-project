import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment('user already login with uncompleted account info and on the main page')

WebUI.callTestCase(findTestCase('Step Definition/TS01 User Login/TS-0101-P user login using newly made account'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('when user want to sell/create a product with incomplete account info')

WebUI.callTestCase(findTestCase('Homepage/page/Click Jual'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('then user will be redirected to detail account info page with a warning notification')

WebUI.callTestCase(findTestCase('Profile/Pages/Verify Content'), [:], FailureHandling.STOP_ON_FAILURE)

