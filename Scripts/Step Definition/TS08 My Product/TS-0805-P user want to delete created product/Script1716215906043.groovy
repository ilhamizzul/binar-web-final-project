import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment('given user on the users\'s own product page')

WebUI.callTestCase(findTestCase('Step Definition/TS08 My Product/TS-0802-P user want to see detailed info of created product'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('when user want to delete a product that user created')

String productName = WebUI.callTestCase(findTestCase('ProductDetail/Pages/Get Product Name'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('ProductDetail/Pages/Click Delete'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('then system successfully delete a product')

WebUI.callTestCase(findTestCase('OwnProduct/Pages/Verify Product Deleted'), [('productName') : productName], FailureHandling.STOP_ON_FAILURE)

