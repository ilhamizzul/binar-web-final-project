import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String charsNumber = '0123456789'

String productName = 'TESTER_PRODUCT_EDITED' + randomString(charsNumber, 5)

String price = '15000'

WebUI.comment('given user on the users\'s own product page')

WebUI.callTestCase(findTestCase('Step Definition/TS08 My Product/TS-0802-P user want to see detailed info of created product'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('when user want to edit product that user created')

WebUI.callTestCase(findTestCase('ProductDetail/Pages/Click Edit'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Nama Produk'), [('productName') : productName], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Harga Produk'), [('productPrice') : price], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CreateProduct/Pages/Select Kategori Produk'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CreateProduct/Pages/Input Deskripsi Produk'), [('productDescription') : 'lorem ipsum dolor sit amet'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CreateProduct/Pages/Click Create Product'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('then user success to edit a product and redirected to product detail')

WebUI.callTestCase(findTestCase('ProductDetail/Pages/verify content own product'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('ProductDetail/Pages/Verify Product Name'), [('productName') : productName], FailureHandling.STOP_ON_FAILURE)

static String randomString(String chars, int length) {
    Random rand = new Random()

    StringBuilder sb = new StringBuilder()

    for (int i = 0; i < length; i++) {
        sb.append(chars.charAt(rand.nextInt(chars.length())))
    }
    
    return sb.toString()
}

