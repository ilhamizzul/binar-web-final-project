import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment('given user already login and on the main page')

WebUI.callTestCase(findTestCase('Step Definition/TS01 User Login/TS-0102-P user login using completed data account'), [('email') : 'user@gmail.com'
        , ('password') : 'user123'], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('when user want to see detailed info of product that user created')

WebUI.callTestCase(findTestCase('OwnProduct/Pages/Open Own Product Page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('OwnProduct/Pages/Click Product'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('then system redirected to product detail that user select')

WebUI.callTestCase(findTestCase('ProductDetail/Pages/verify content own product'), [:], FailureHandling.STOP_ON_FAILURE)

