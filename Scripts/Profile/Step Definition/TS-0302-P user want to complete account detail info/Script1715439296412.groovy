import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/Pages/Open Login Page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Login/Pages/Input Email'), [('email') : 'zakkimubarak03@gmail.com'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Login/Pages/Input Password'), [('password') : 'zakkibarca0310'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Login/Pages/Click Submit Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Login/Pages/Verify Login Success'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.callTestCase(findTestCase('Profile/Pages/Click Profile'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profile/Pages/Verify Menu Profile'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.callTestCase(findTestCase('Profile/Pages/Upload Image'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebUI.callTestCase(findTestCase('Profile/Pages/Input Nama'), [('namalogin') : 'zakki'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profile/Pages/Input Kota'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profile/Pages/Input Alamat'), [('alamatlogin') : 'jl. bunga, kembangan, jakarta barat'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profile/Pages/Input Nomor Handphone'), [('handphonelogin') : '083807969098'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profile/Pages/Click Button Simpan'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profile/Pages/Verify Success'), [:], FailureHandling.STOP_ON_FAILURE)

